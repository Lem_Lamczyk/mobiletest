//
//  RepositoryCellConfigurator.h
//  mobileTest
//
//  Created by Mateusz Lamczyk on 15/12/2016.
//  Copyright © 2016 Mateusz Lamczyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"
#import "RepositorySection.h"

@interface RepositoryCellConfigurator : NSObject

- (void)registerNibsForCollectionView:(UICollectionView *)collectionView;
- (NSString *)cellIdentifierForSectionType:(RepositorySectionType)sectionType;
- (void)configureCell:(UICollectionViewCell *)cell forSection:(RepositorySection *)section atIndexPath:(NSIndexPath *)indexPath;
- (CGSize)sizeForObjectInCollectionView:(UICollectionView *)collectionView forSection:(RepositorySection *)section atIndexPath:(NSIndexPath *)indexPath;

@end
