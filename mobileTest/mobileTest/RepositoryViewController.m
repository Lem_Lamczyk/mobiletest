//
//  ViewController.m
//  mobileTest
//
//  Created by Mateusz Lamczyk on 15/12/2016.
//  Copyright © 2016 Mateusz Lamczyk. All rights reserved.
//

#import "RepositoryViewController.h"
#import "SearchBarController.h"
#import "RepositoryViewModel.h"
#import "RepositoryCollectionViewDataSource.h"
#import "MBProgressHUD.h"

static CGFloat const kTimerInterval = 0.4;
static NSString * const kSearchBarSegueIdentifier = @"SearchBar";
static NSString * const kTetrisQuery = @"tetris";

@interface RepositoryViewController () <RepositoryViewModelDelegate, SearchBarControllerDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) SearchBarController *searchBarController;
@property (strong, nonatomic) RepositoryViewModel *viewModel;
@property (strong, nonatomic) RepositoryCollectionViewDataSource *dataSource;
@property (strong, nonatomic) NSTimer * searchTimer;

@end

@implementation RepositoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataSource = [[RepositoryCollectionViewDataSource alloc] init];
    [self.dataSource.cellConfigurator registerNibsForCollectionView:self.collectionView];
    self.viewModel = [[RepositoryViewModel alloc] init];
    self.viewModel.delegate = self;
    [self.viewModel loadDataWithQuery:kTetrisQuery];
    self.collectionView.delegate = self.dataSource;
    self.collectionView.dataSource = self.dataSource;
    self.collectionView.emptyDataSetSource = self.dataSource;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(deviceOrientationDidChange:)    name:UIDeviceOrientationDidChangeNotification  object:nil];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}

- (void)deviceOrientationDidChange:(NSNotification *)notification{
    [self.collectionView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kSearchBarSegueIdentifier]) {
        self.searchBarController = (SearchBarController*)segue.destinationViewController;
        self.searchBarController.delegate = self;
    }
}

#pragma mark - RepositoryViewModel delegate methods

- (void)repositoryViewModel:(RepositoryViewModel *)repositoryViewModel didLoadData:(NSArray *)sections {
    [MBProgressHUD hideHUDForView:self.collectionView animated:YES];
    self.dataSource.sections = sections;
    [self.collectionView reloadData];
}

- (void)repositoryViewModel:(RepositoryViewModel *)repositoryViewModel didFailWithError:(NSError *)error {
    [MBProgressHUD hideHUDForView:self.collectionView animated:YES];
    self.dataSource.sections = nil;
    [self.collectionView reloadEmptyDataSet];
    [self.collectionView reloadData];
}

- (void)repositoryViewModelDidStartLoading:(RepositoryViewModel *)repositoryViewModel {
    [MBProgressHUD showHUDAddedTo:self.collectionView animated:YES];
}

#pragma mark - SearchBarController delegate methods

- (void)searchBarController:(SearchBarController *)searchBarController textFieldDidChange:(UITextField *)textField {
    if (self.searchTimer != nil) {
        [self.searchTimer invalidate];
        self.searchTimer = nil;
    }

    self.searchTimer = [NSTimer scheduledTimerWithTimeInterval: kTimerInterval target: self selector: @selector(searchForKeyword:)userInfo: textField.text repeats: NO];
}

- (void)searchForKeyword:(NSTimer *)timer {
    NSString *text = (NSString*)timer.userInfo;
    if (text.length > 0) {
        [self.viewModel loadDataWithQuery:text];
    } else {
        [self.viewModel loadDataWithQuery:kTetrisQuery];
    }
}

@end
