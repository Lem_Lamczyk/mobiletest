//
//  SearchBarController.h
//  mobileTest
//
//  Created by Mateusz Lamczyk on 15/12/2016.
//  Copyright © 2016 Mateusz Lamczyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchBarTextField.h"

@class SearchBarController;

@protocol SearchBarControllerDelegate <NSObject>
@optional
- (void)searchBarController:(SearchBarController*)searchBarController textFieldDidChange:(UITextField*)textField;
- (void)searchBarController:(SearchBarController*)searchBarController textFieldDidBeginEditing:(UITextField*)textField;
- (void)searchBarController:(SearchBarController*)searchBarController textFieldShouldReturn:(UITextField*)textField;

@end

@interface SearchBarController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) id<SearchBarControllerDelegate> delegate;

@end
