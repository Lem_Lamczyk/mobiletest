//
//  SearchBarController.m
//  mobileTest
//
//  Created by Mateusz Lamczyk on 15/12/2016.
//  Copyright © 2016 Mateusz Lamczyk. All rights reserved.
//

#import "SearchBarController.h"

static NSString * const kSearchBarControllerNibName = @"SearchBarController";

@interface SearchBarController ()
@property (weak, nonatomic) IBOutlet SearchBarTextField *searchBarTextField;

@end

@implementation SearchBarController

- (instancetype)init {
    self = [super init];
    if (self) {
        [[NSBundle mainBundle] loadNibNamed:kSearchBarControllerNibName owner:self options:nil];
        [self _commonInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [[NSBundle mainBundle] loadNibNamed:kSearchBarControllerNibName owner:self options:nil];
        [self _commonInit];
    }
    return self;
}

- (void)_commonInit {
    self.searchBarTextField.delegate = self;
    
    [self.view.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.view.layer setShadowOpacity:0.8];
    [self.view.layer setShadowRadius:5.0];
    [self.view.layer setShadowOffset:CGSizeZero];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - UITextField delegate methods

- (IBAction)textFieldDidChange:(UITextField *)textField {
    if ([self.delegate respondsToSelector:@selector(searchBarController:textFieldDidChange:)]) {
        [self.delegate searchBarController:self textFieldDidChange:textField];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [textField becomeFirstResponder];
    if ([self.delegate respondsToSelector:@selector(searchBarController:textFieldDidBeginEditing:)]) {
        [self.delegate searchBarController:self textFieldDidBeginEditing:textField];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if ([self.delegate respondsToSelector:@selector(searchBarController:textFieldShouldReturn:)]) {
        [self.delegate searchBarController:self textFieldShouldReturn:textField];
    }
    return YES;
}

@end
