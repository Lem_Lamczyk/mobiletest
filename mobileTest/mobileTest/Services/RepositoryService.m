//
//  RepositoryService.m
//  mobileTest
//
//  Created by Mateusz Lamczyk on 16/12/2016.
//  Copyright © 2016 Mateusz Lamczyk. All rights reserved.
//

#import "RepositoryService.h"

static NSString * const kRepositoriesURL = @"https://api.github.com/search/repositories?q=";

@implementation RepositoryService

- (void)getRepositoryInfoWithQuery:(NSString *)query completion:(void (^)(NSDictionary * _Nullable, NSError * _Nullable))completion {
    NSError *error;
    NSString *url_string = [NSString stringWithFormat: @"%@%@",kRepositoriesURL, query];
    NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:url_string]];
    if (!data) {
        completion(nil, error);
        return;
    }
    NSDictionary *output = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if (!output) {
        completion(nil, error);
        return;
    }
    
    completion(output, nil);
}

@end
