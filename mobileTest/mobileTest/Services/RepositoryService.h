//
//  RepositoryService.h
//  mobileTest
//
//  Created by Mateusz Lamczyk on 16/12/2016.
//  Copyright © 2016 Mateusz Lamczyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RepositoryService : NSObject

- (void)getRepositoryInfoWithQuery:(NSString* _Nullable)query completion:(nullable void (^)(NSDictionary * _Nullable, NSError * _Nullable))completion;

@end
