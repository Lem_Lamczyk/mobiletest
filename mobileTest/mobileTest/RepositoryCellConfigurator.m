//
//  RepositoryCellConfigurator.m
//  mobileTest
//
//  Created by Mateusz Lamczyk on 15/12/2016.
//  Copyright © 2016 Mateusz Lamczyk. All rights reserved.
//

#import "RepositoryCellConfigurator.h"
#import "RepositoryCollectionViewCell.h"

static CGFloat const kDefaultGithubCellHeight = 70;

@implementation RepositoryCellConfigurator

- (NSDictionary *)cellIdentifierForSections {
    return @{@(RepositorySectionTypeGithub) :   NSStringFromClass([RepositoryCollectionViewCell class])
             };
}

- (NSString*)cellIdentifierForSectionType:(RepositorySectionType)sectionType {
    NSString *cellIdentifier = [self cellIdentifierForSections][@(sectionType)];
    NSParameterAssert(cellIdentifier);
    return cellIdentifier;
}

- (void)registerNibsForCollectionView:(UICollectionView *)collectionView {
    for (NSString *identifier in [self cellIdentifierForSections].allValues) {
        [collectionView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellWithReuseIdentifier:identifier];
    }
}

- (void)configureCell:(UICollectionViewCell *)cell forSection:(RepositorySection *)section atIndexPath:(NSIndexPath *)indexPath {
    RepositoryRow *row = section.rows[indexPath.item];
    switch (section.sectionType) {
        case RepositorySectionTypeGithub: [self configureRepositoryGithubCell:(RepositoryCollectionViewCell*)cell withRepositoryRow:row];
            break;
        default: NSAssert(false, @"Unknown section type");
            break;
    }
}

- (CGSize)sizeForObjectInCollectionView:(UICollectionView *)collectionView forSection:(RepositorySection *)section atIndexPath:(NSIndexPath *)indexPath {
    switch (section.sectionType) {
        case RepositorySectionTypeGithub: return CGSizeMake([self fullScreenCellWidthInCollectionView:collectionView], kDefaultGithubCellHeight);
        default: NSAssert(false, @"Unknown section type");
            return CGSizeZero;
    }
}

- (CGFloat)fullScreenCellWidthInCollectionView:(UICollectionView*)collectionView {
    return collectionView.bounds.size.width;
}

- (void)configureRepositoryGithubCell:(RepositoryCollectionViewCell*)cell withRepositoryRow:(RepositoryRow*)row {
    cell.repositoryName.text = row.repositoryName;
    cell.loginName.text = row.loginName;
    cell.repositorySize.text = row.repositorySize;
    if (row.has_wiki) {
        cell.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0];
    } else {
        cell.backgroundColor = [UIColor whiteColor];
    }
}

@end
