//
//  SearchBarTextField.h
//  mobileTest
//
//  Created by Mateusz Lamczyk on 15/12/2016.
//  Copyright © 2016 Mateusz Lamczyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchBarTextField : UITextField

@end
