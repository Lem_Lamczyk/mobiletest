//
//  RepositorySectionBuilder.h
//  mobileTest
//
//  Created by Mateusz Lamczyk on 16/12/2016.
//  Copyright © 2016 Mateusz Lamczyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RepositorySection.h"

@interface RepositorySectionBuilder : NSObject

- (NSArray<RepositorySection*>*)buildRepositorySectionsWithOutput:(NSDictionary *)output supportedKeys:(NSArray<NSString*>*)supportedKeys;

@end
