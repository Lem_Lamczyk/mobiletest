//
//  RepositoryViewModel.m
//  mobileTest
//
//  Created by Mateusz Lamczyk on 15/12/2016.
//  Copyright © 2016 Mateusz Lamczyk. All rights reserved.
//

#import "RepositoryViewModel.h"
#import "RepositorySectionBuilder.h"
#import "RepositoryService.h"

@interface RepositoryViewModel()

@property (nonatomic, strong) RepositorySectionBuilder *sectionBuilder;
@property (strong, nonatomic) RepositoryService *repositoryService;
@property (strong, nonatomic) NSArray<NSString*> *supportedKeys;

@end

@implementation RepositoryViewModel

- (instancetype)init {
    self = [super init];
    if (self) {
        self.sectionBuilder = [[RepositorySectionBuilder alloc] init];
        self.repositoryService = [[RepositoryService alloc] init];
        self.supportedKeys = @[@"name", @"owner", @"size", @"has_wiki"];
    }
    return self;
}

- (void)loadDataWithQuery:(NSString*)query {
    if ([self.delegate respondsToSelector:@selector(repositoryViewModelDidStartLoading:)]) {
        [self.delegate repositoryViewModelDidStartLoading:self];
    }
    
    [self.repositoryService getRepositoryInfoWithQuery:query completion:^(NSDictionary * output, NSError * error) {
        if (!output) {
            if ([self.delegate respondsToSelector:@selector(repositoryViewModel:didFailWithError:)]) {
                [self.delegate repositoryViewModel:self didFailWithError:error];
            }
            return;
        }
        
        self.sections = [self.sectionBuilder buildRepositorySectionsWithOutput:output supportedKeys:self.supportedKeys];
        if ([self.delegate respondsToSelector:@selector(repositoryViewModel:didLoadData:)]) {
            [self.delegate repositoryViewModel:self didLoadData:self.sections];
        }
    }];
}

@end
