//
//  RepositoryRow.h
//  mobileTest
//
//  Created by Mateusz Lamczyk on 15/12/2016.
//  Copyright © 2016 Mateusz Lamczyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RepositoryRow : NSObject

@property (copy, nonatomic) NSString *repositoryName;
@property (copy, nonatomic) NSString *loginName;
@property (copy, nonatomic) NSString *repositorySize;
@property (assign, nonatomic) BOOL has_wiki;

@end
