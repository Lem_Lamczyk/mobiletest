//
//  RepositorySection.h
//  mobileTest
//
//  Created by Mateusz Lamczyk on 15/12/2016.
//  Copyright © 2016 Mateusz Lamczyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RepositoryRow.h"

typedef enum {
    RepositorySectionTypeGithub,
    RepositorySectionTypeDefault
} RepositorySectionType;

@interface RepositorySection : NSObject

@property (strong, nonatomic) NSArray<RepositoryRow*> *rows;
@property (assign, nonatomic) RepositorySectionType sectionType;

@end
