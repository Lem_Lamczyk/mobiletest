//
//  RepositorySectionBuilder.m
//  mobileTest
//
//  Created by Mateusz Lamczyk on 16/12/2016.
//  Copyright © 2016 Mateusz Lamczyk. All rights reserved.
//

#import "RepositorySectionBuilder.h"
#import "RepositoryRow.h"

@implementation RepositorySectionBuilder

- (NSArray<RepositorySection *> *)buildRepositorySectionsWithOutput:(NSDictionary *)output supportedKeys:(NSArray<NSString*>*)supportedKeys{
    RepositorySection *repositorySection = [[RepositorySection alloc] init];
    repositorySection.sectionType = RepositorySectionTypeGithub;
    
    NSMutableArray *rows = [NSMutableArray array];
    NSDictionary *items = output[@"items"];
    BOOL containsAllTheNeededKeys = true;
    for (NSDictionary *dict in items) {
        for (NSString* key in supportedKeys) {
            if (![[dict allKeys] containsObject:key]) {
                containsAllTheNeededKeys = false;
            }
        }

        if (containsAllTheNeededKeys) {
            RepositoryRow *row = [[RepositoryRow alloc] init];
            for (NSString* key in supportedKeys) {
                if ([key isEqualToString:@"name"]) {
                    NSString *repositoryName = [dict objectForKey:key];
                    row.repositoryName = repositoryName;
                } else if ([key isEqualToString:@"size"]) {
                    NSNumber *repositorySize = [dict objectForKey:key];
                    row.repositorySize = [repositorySize stringValue];
                } else if ([key isEqualToString:@"has_wiki"]) {
                    BOOL has_wiki = [[dict objectForKey:key] boolValue];
                    row.has_wiki = has_wiki;
                } else if ([key isEqualToString:@"owner"]) {
                    NSDictionary *owner = [dict objectForKey:key];
                    for (NSString* ownerKey in owner) {
                        if ([ownerKey isEqualToString:@"login"]) {
                            NSString *loginName = [owner objectForKey:ownerKey];
                            row.loginName = loginName;
                            break;
                        }
                    }
                }
            }
            [rows addObject:row];
        }
    }
    
    repositorySection.rows = rows;
    return @[repositorySection];
}

@end
