//
//  RepositoryViewModel.h
//  mobileTest
//
//  Created by Mateusz Lamczyk on 15/12/2016.
//  Copyright © 2016 Mateusz Lamczyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RepositorySection.h"

@class RepositoryViewModel;

@protocol RepositoryViewModelDelegate <NSObject>

- (void)repositoryViewModel:(RepositoryViewModel*)repositoryViewModel didLoadData:(NSArray *)sections;
- (void)repositoryViewModel:(RepositoryViewModel *)repositoryViewModel didFailWithError:(NSError *)error;
- (void)repositoryViewModelDidStartLoading:(RepositoryViewModel*)repositoryViewModel;

@end

@interface RepositoryViewModel : NSObject

@property (strong, nonatomic) NSArray<RepositorySection*> *sections;
@property (weak, nonatomic) id<RepositoryViewModelDelegate> delegate;

- (void)loadDataWithQuery:(NSString*)query;

@end
