//
//  RepositoryCollectionViewDataSource.m
//  mobileTest
//
//  Created by Mateusz Lamczyk on 15/12/2016.
//  Copyright © 2016 Mateusz Lamczyk. All rights reserved.
//

#import "RepositoryCollectionViewDataSource.h"


@implementation RepositoryCollectionViewDataSource

- (instancetype)init {
    self = [super init];
    if (self) {
        self.cellConfigurator = [[RepositoryCellConfigurator alloc] init];
    }
    return self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section >= self.sections.count) {
        return 0;
    }
    RepositorySection *repositorySection = self.sections[section];
    return repositorySection.rows.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RepositorySection *repositorySection = self.sections[indexPath.section];
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[self.cellConfigurator cellIdentifierForSectionType:repositorySection.sectionType] forIndexPath:indexPath];
    [self.cellConfigurator configureCell:cell forSection:repositorySection atIndexPath:indexPath];    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section >= self.sections.count) {
        return CGSizeZero;
    }
    RepositorySection *repositorySection = self.sections[indexPath.section];
    return [self.cellConfigurator sizeForObjectInCollectionView:collectionView forSection:repositorySection atIndexPath:indexPath];
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Oh no!\nData is missing!", nil)];
}

@end
