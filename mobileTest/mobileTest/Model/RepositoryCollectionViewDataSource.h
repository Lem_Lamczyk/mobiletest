//
//  RepositoryCollectionViewDataSource.h
//  mobileTest
//
//  Created by Mateusz Lamczyk on 15/12/2016.
//  Copyright © 2016 Mateusz Lamczyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#import "UIKit/UIKit.h"
#import "RepositoryCellConfigurator.h"
#import "RepositorySection.h"

@interface RepositoryCollectionViewDataSource : NSObject <UICollectionViewDelegate, UICollectionViewDataSource, DZNEmptyDataSetSource>

@property (strong, nonatomic) NSArray<RepositorySection*> *sections;
@property (strong, nonatomic) RepositoryCellConfigurator *cellConfigurator;

@end
